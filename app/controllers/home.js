/*!
 * controller home
 *   
 *  @package     /app/controllers/
 *  @author      Luan Rafael <luanrafaelcastor@gmail.com>
 *  @copyright   lrpinheiro.tk
 *  @link        /app/controllers/home.js
 *  @since      1.0.0
 */

 module.exports = () => {
 	var controller = {};

 	controller.index =  (req, res) => {
 		res.render('index');
 	};

 	controller.teste =  (req, res) => {
 		res.render('teste');
 	};

 	controller.render =  (req, res) => {
 		var view = req.params.view;
 		res.render(view);
 	};

 	return controller;
 }
