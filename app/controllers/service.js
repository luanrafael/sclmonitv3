/*!
 * controller home
 *   
 *  @package     controllers/service.js
 *  @author      Luan Rafael <luanrafaelcastor@gmail.com>
 *  @copyright   lrpinheiro.tk
 *  @link        controllers/service
 *  @since       0.0.1
 */


module.exports = (app) => {
 	var controller = {};
 	var Service = app.models.service;
 	var ObjectId = app.mongoose.Types.ObjectId;
 	controller.list = (req, res) => {
 		var promisse = Service.find({'active': true});
 		promisse.then(
 			(services) => {
	 			res.status(200).json(services);
	 		},
	 		(err) => {
	 			res.status(500).json(err);
	 	});
 	};

 	controller.find = (req, res) => {
 		
 		var id = req.params.id;
 		
 		Service.findById(id, (err, service) => {
 			
 			if(err) {
	 			res.send(404, err);
	 			return;
 			}
 			console.log(service);
 			if(!service) {
 				res.status(500).json({error: true});
 				return;
 			}
 			
 			res.status(200).json(service);
	 		
 		});
 		 		
 	};

 	controller.remove = (req, res) => {

 		var id = req.params.id;
 		var query = {'_id': id};
 		var promisse = Service.remove(query);

 		promisse.then( 
 			() => {
 				res.send(204);
 			},
 			(err) => {
 				res.send(500).json(err);
 			}
 		);
 	}

 	controller.save = (req, res) => {

 		var id = req.params.id;

 		if(id) {
 			var data_upd = req.body;
 			data_upd.updated = Date.now();
 			var promisse = Service.findByIdAndUpdate({_id: id}, data_upd);
 			promisse.then( 
	 			(service) => {
	 				res.json(service);
	 			},
	 			(err) => {
	 				res.status(500).json(err);
	 			}
 			);
 		} else {
 			var promisse = Service.create(req.body);
 			promisse.then( 
	 			(service) => {
	 				res.status(201).json(service);
	 			},
	 			(err) => {
	 				res.status(500).json(err);
	 			}
 			);
 		}

 	}

 	controller.verify = (req, res) => {

 		var promisse = Service.find({'active': true});

 		promisse.then(
 			(services) => {
	 			res.json({message: 'Verificando'});
	 			app.services.monitor.verify(services);
	 		},
	 		(err) => {
	 			res.status(500).json(err);
	 	});

 	}

	controller.ping = (req, res) => {

 		var id = req.params.id;

 		if(!id) res.status(404).json({message: 'servico nao encontrado'});
 		
 		var data_upd = {  
 			updated: Date.now(),
 			config: { last_ping: Date.now() } ,
 			status: 1,
 			qtd_errors: 0
 		}

 		var promisse = Service.findByIdAndUpdate({_id: id}, data_upd);

		promisse.then( 
			(service) => {
				res.json(service);
			},
			(err) => {
				res.status(500).json(err);
			}
		);

 	}

 	controller.active_all = (req, res) => {
 		var promisse = Service.update({},{"$set":{active:true}},{ multi: true }).exec()
 		promisse.then(
 			(services) => {
	 			res.status(200).json(services);
	 		},
	 		(err) => {
	 			res.status(500).json(err);
	 	});
 	}

 	return controller;
 }