/*!
 * route /service
 *   
 *  @package     routes/service.js
 *  @author      Luan Rafael <luanrafaelcastor@gmail.com>
 *  @copyright   lrpinheiro.tk
 *  @link        routes/service.js
 *  @since      1.0.0
 */


module.exports = (app) => {
	var controller = app.controllers.service;
	
	app.route('/services')
		.get(controller.list)
		.post(controller.save);

	app.route('/verify')
		.get(controller.verify);

	app.route('/ping/:id')
		.put(controller.ping)
		.get(controller.ping)
		.post(controller.ping);

	app.route('/services/:id')
		.get(controller.find)
		.delete(controller.remove)
		.put(controller.save);

	app.route('/active_all')
		.get(controller.active_all);
}