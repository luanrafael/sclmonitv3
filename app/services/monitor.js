/*!
 *  Arquivo responsavel pelo monitoramento dos servicos ativos
 *   
 *  @package     services
 *  @author      Luan Rafael <luan@epsoft.com.br>
 *  @copyright   SuperClient Solutions LTDA
 *  @link        link
 *  @since       1.0.0
 */


var mailgun = require('mailgun-js')({apiKey: 'key-728d984330524a548b6cb1f19fab0079', domain: 'kanban360.com.br'});


module.exports = (app) => {

	var Service = app.models.service;

	var monitor = app.monitors;

	var monitor_manager = {};	

	monitor_manager.verify = (data) => {


		for(var i = 0; i < data.length; i++){

			var register = data[i];
			var type = register.type.toLowerCase().trim();
			
			if(type in monitor)
				monitor[type].verify(register, monitor_manager.check_result);
			else
				console.error(type, 'não existe!');

		}

	}

	monitor_manager.check_result = (err, result) => {

		console.log(err, result);

		if(err && !result.id ) {
			throw err;
			return;
		}

	
		if((err && result.id) || result.error) {
			

			var promisse = Service.findById(result.id);
	 		promisse.then(
	 			(service) => {
		 			if(!service) throw new Error('Service not found');

		 			service.qtd_errors += 1;

		 			if(service.qtd_errors >= service.alert){
		 				service.status = 0;

		 				var send_alert = false;

						if(service.qtd_alert <= 10){
							
							var diff = Date.now() - (service.last_alert||0);
							var diff_minutes = Math.floor(diff / 1000 / 60);

							if(diff_minutes > 60) {
								send_alert = true;
							}

						} else {
							send_alert = true;
						}
		 				
						if(send_alert){
		 					monitor_manager.send_email(service);
							service.last_alert = Date.now();
							service.qtd_alert += 1;
						}

						
		 			}

		 			monitor_manager.upd_service(service);

		 		},
		 		(err) => {
		 			throw err;
		 		}
		 	);

	 		return;

		}

		if(!err) {
			Service.findById(result.id, (err, service) => {
					
					if(err) throw err;

		 			if(!service) throw new Error('Service not found');

		 			service.qtd_errors = 0;
	 				service.status = 1;
	 				service.qtd_alert = 0;
	 				service.last_alert = null;

		 			monitor_manager.upd_service(service);
			});

		}

	}

	monitor_manager.upd_service = (service) => {
		service.updated = Date.now();
		var promisse = Service.findByIdAndUpdate({_id: service.id}, service).exec();

		promisse.then( 
 			(service) => {
 				console.log('atualizei');
 			},
 			(err) => {
 				console.log(err);
 			}
		);
	}

	monitor_manager.send_email = (service) => {


		var message = 'servico: ' + service.name;
		message += '\nid: ' + service.id;
		message += '\nerrors: ' + service.qtd_errors;

		var data = {
			from: 'Monitor SuperClient <superclient@superclient.com.br>',
		  	to: 'superclient@superclient.com.br',
		  	subject: 'Monitor - SCL',
		  	text: message
		};
		 
		mailgun.messages().send(data, function (error, body) {
			console.log(body);
		});
	}

	return monitor_manager;
};
