/*!
 *  description
 *   
 *  @package     server.js
 *  @author      Luan Rafael <luanrafaelcastor@gmail.com>
 *  @copyright   lrpinheiro.tk
 *  @link        path
 *  @since      1.0.0
 */


var http = require('http');
var config = require('./config/config')();
Object.assign=require('object-assign')
var db = require('./config/database')(config.db);

var app = require('./config/express')(db);

app.listen(config.port, config.address)

console.log('Express server escutando na porta: ', config.port);

module.exports = app ;