
angular.module('scl_monitor').controller('ServicesController', 
	function($scope, scl_monitor_api){

		$scope.total = 0;

		scl_monitor_api.getServices().success(function(result){
			append_result(result);
		});

		var icons = {
			0: {
				icon: 'error_outline',
				color: 'red',
			},
			1: {
				icon: 'thumb_up',
				color: 'green',
			},
			2: {
				icon: 'thumb_up',
				color: 'amber',
			},
		}

		function append_result(services){

			for (var i = services.length - 1; i >= 0; i--) {
				services[i].icon = icons[services[i].status]
				services[i].updated = (new Date(services[i].updated)).toLocaleString();
			}

			$scope.services = services;

		}

	}
);