/*!
 * arquivo de configuração do banco!
 *   
 *  @package     /config
 *  @author      Luan Rafael <luanrafaelcastor@gmail.com>
 *  @copyright   lrpinheiro.tk
 *  @link        /config/databse.js
 *  @since       1.0.0
 */


 var mongoose = require('mongoose');


 module.exports = (uri) => {
 	mongoose.connect(uri, {
 		useMongoClient: true,
 		promiseLibrary: require('bluebird')
 	});

 	mongoose.connection.on('connected', () => {
 		console.log('Mongoose! Conectado em ', uri);
 	});

 	mongoose.connection.on('disconnected', () => {
 		console.log('Mongoose! Desconectado de ', uri);
 	});

 	mongoose.connection.on('error', (err) => {
 		console.log('Mongoose! Erro na conexão: ', err);
 	});

 	process.on('SIGINT',() => {
 		mongoose.connection.close(() => {
 			console.log('Mongoose! Desconectado pelo térmido da aplicação!');
 			process.exit(0);
 		});
 	});

 	mongoose.set('debug',true);

 	return mongoose;
 }