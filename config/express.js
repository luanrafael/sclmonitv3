/*!
 *  Arquivo de configuração do express
 *   
 *  @package     config/express.js
 *  @author      Luan Rafael <luanrafaelcastor@gmail.com>
 *  @copyright   lrpinheiro.tk
 *  @link        path
 *  @since      1.0.0
 */


var express = require('express');
var load = require('express-load');
var body_parser = require('body-parser');


module.exports = (db) => {
	var app = express();
	app['mongoose'] = db;
	//setando porta
	/*app.set('port', config.application.port);
	app.set('mongodb_url_connection', config.database.url);
*/
	//middlewares
	app.use(express.static('./public'));
	app.use(body_parser.urlencoded({extended: true}));
	app.use(body_parser.json());
	app.use(require('method-override')());

	//setando views
	app.set('view engine', 'ejs');
	app.set('views', './app/views');

	app.use(function(err, req, res, next){
	  console.error(err.stack);
	  res.status(500).send('Something bad happened!');
	});
	
	load('models', {cwd: 'app'})
		.then('monitors')
		.then('services')
		.then('controllers')
		.then('routes')
		.into(app);
	

	return app;
}
